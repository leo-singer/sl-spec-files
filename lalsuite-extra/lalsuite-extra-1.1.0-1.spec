%define _prefix /usr

Name: lalsuite-extra
Version: 1.1.0
Release: 1%{?dist}
Summary: LIGO Algorithm Library Extra Data
License: GPL
Group: LAL
Source: %{name}-%{version}.tar.gz
URL: https://wiki.ligo.org/DASWG/LALSuite
Packager: Jolien Creighton <jolien.creighton@ligo.org>
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Requires: lal lalsimulation
Prefix: %{_prefix}

%description
Data for the LIGO Algorithm Simulation Library for gravitational wave data
analysis. This package contains extra data files needed to run certain
applications that use the LAL Simulation library.

%prep
%setup -q

%build
%configure
%{__make} V=1

%install
%make_install

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}

%files
%defattr(-,root,root)
%{_datarootdir}/lalsimulation/SEOBNRv1ROM_DS_*.dat
%{_datarootdir}/lalsimulation/SEOBNRv1ROM_SS_*.dat
%{_datarootdir}/lalsimulation/SEOBNRv2ChirpTimeSS.dat
%{_datarootdir}/lalsimulation/SEOBNRv2ROM_DS_*.dat
%{_datarootdir}/lalsimulation/SEOBNRv2ROM_SS_*.dat
