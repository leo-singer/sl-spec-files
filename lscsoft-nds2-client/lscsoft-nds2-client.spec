Name: lscsoft-nds2-client
Version: 1.1
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: NDS2 Client LSCSoft meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: nds2-client
Requires: nds2-client-devel
Requires: nds2-client-doc
Requires: nds2-client-headers
Requires: nds2-client-java
Requires: nds2-client-man
Requires: nds2-client-octave
Requires: nds2-client-python

%description
This package is a meta-package to ensure the installation of the
LSCSoft NDS2 Client packages.

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Wed Apr 20 2016 Adam Mercer <adam.mercer@ligo.org> 1.1-1
- remove dependencies on debuginfo packages

* Fri Aug 28 2015 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial version
