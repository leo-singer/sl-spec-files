Name: lscsoft-ldas
Version: 1.6
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: LDASTools meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: ldas-tools-al
Requires: ldas-tools-al-devel
Requires: ldas-tools-diskcacheAPI
Requires: ldas-tools-diskcacheAPI-devel
Requires: ldas-tools-diskcacheAPI-python
Requires: ldas-tools-filters
Requires: ldas-tools-filters-devel
Requires: ldas-tools-frameAPI
Requires: ldas-tools-frameAPI-devel
Requires: ldas-tools-frameAPI-python
Requires: ldas-tools-framecpp
Requires: ldas-tools-framecpp-c
Requires: ldas-tools-framecpp-c-devel
Requires: ldas-tools-framecpp-devel
Requires: ldas-tools-framecpp-doc
Requires: ldas-tools-framecpp-python
Requires: ldas-tools-ldasgen
Requires: ldas-tools-ldasgen-devel
Requires: ldas-tools-ldasgen-python
Requires: ldas-tools-utilities

%description
This package is a meta-package to ensure the installation of the
LDASTools LSCSOFT packages.

%install

%files

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Thu Apr 14 2016 Adam Mercer <adam.mercer@ligo.org> 1.6-1
- update for new ldas-tools package names

* Wed Aug 19 2015 Adam Mercer <adam.mercer@ligo.org> 1.5-1
- initial version
