%global upname husl

Name: python-%{upname}
Version: 4.0.3
Release: 2.lscsoft%{?dist}
Summary: A Python implementation of HUSL
License: MIT

URL: http://github.com/boronine/pyhusl
Source0: https://pypi.python.org/packages/source/h/husl/husl-%{version}.tar.gz
# By mistake the license is not packaged (fixed in devel version upstream)
Source1: https://raw.githubusercontent.com/husl-colors/husl-python/master/LICENSE.txt
BuildArch: noarch
BuildRequires: python2-devel python-setuptools
#BuildRequires: python3-devel python3-setuptools

%description
HUSL is a human-friendly alternative to HSL (Hue, Saturation and Lightness)
color space. This package provides Python2 support

#%package -n python3-%{upname}
#Summary: A Python implementation of HUSL
#BuildRequires: python3-devel python3-setuptools

#%description -n python3-%{upname}
#HUSL is a human-friendly alternative to HSL (Hue, Saturation and Lightness)
#color space. This package provides Python3 support


%prep
%setup -q -n %{upname}-%{version}

find -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python}|'

#rm -rf %{py3dir}
#cp -a . %{py3dir}
#find %{py3dir} -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'

%build
cp -p %{SOURCE1} .

%{__python2} setup.py build

#pushd %{py3dir}
#%{__python3} setup.py build
#popd


%install
#pushd %{py3dir}
#%{__python3} setup.py install --skip-build --root %{buildroot}
#popd

%{__python2} setup.py install --skip-build --root %{buildroot}

%files
%doc README.md
%license LICENSE.txt
%{python2_sitelib}/husl*

#%files -n python3-%{upname}
#%doc README.md
#%license LICENSE.txt
#%{python3_sitelib}/husl*
#%{python3_sitelib}/__pycache__/*

%changelog
* Thu Dec 15 2016 Adam Mercer <adam.mercer@ligo.org> 4.0.3-2.lscsoft
- comment out python3 packages

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.0.3-2
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Sat Jul 02 2016 José Abílio Matos <jamatos@fc.up.pt> - 4.0.3-1
- update to 4.0.3

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 4.0.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.0.1-3
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.0.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Fri Nov 21 2014 Sergio Pascual <sergiopr at fedoraproject.com> - 4.0.1-1
- Initial SPEC
