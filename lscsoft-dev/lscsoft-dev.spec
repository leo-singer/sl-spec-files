Name: lscsoft-dev
Version: 1.0
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: Core LSCSoft development meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: abi-compliance-checker
Requires: abi-dumper

%description
This package is a meta-package to ensure the installation of general
packages required for the development of LSC Software

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Tue Nov 28 2017 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial version
