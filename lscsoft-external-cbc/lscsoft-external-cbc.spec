Name: lscsoft-external-cbc
Version: 2.13
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: LSCSoft External CBC meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: cfitsio
Requires: cfitsio-devel
Requires: cfitsio-static
Requires: chealpix-devel
Requires: fftw
Requires: fftw-devel
Requires: fftw-static
Requires: gsl
Requires: gsl-devel
Requires: healpix_cxx-devel
Requires: healpy
Requires: lapack-devel
Requires: numpy
Requires: pegasus
Requires: python-decorator
Requires: python-decoratortools
Requires: python-matplotlib
Requires: python-sqlobject
Requires: python-virtualenv
Requires: scipy
Requires: spr
Requires: spr-devel

%description
This package is a meta-package to ensure the installation of  external
packages required by the CBC LSCSoft codes.

%install

%files

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Wed Jan 17 2018 Adam Mercer <adam.mercer@ligo.org> 2.13-1
- add lapack-devel
- https://bugs.ligo.org/redmine/issues/6070

* Mon Apr 18 2016 Adam Mercer <adam.mercer@ligo.org> 2.12-1
- remove python-sqlite2

* Fri Apr 15 2016 Adam Mercer <adam.mercer@ligo.org> 2.11-1
- remove dependencies on debuginfo packages

* Fri Oct 09 2015 Adam Mercer <adam.mercer@ligo.org> 2.10-2
- add healpy-debuginfo

* Wed Aug 19 2015 Adam Mercer <adam.mercer@ligo.org> 2.10-1
- initial version
