Name: lscsoft-gds
Version: 1.4
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: GDS meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: gds-core
Requires: gds-crtools
Requires: gds-devel
Requires: gds-frameio
Requires: gds-lowlatency
Requires: gds-monitors
Requires: gds-pygds
Requires: gds-root
Requires: gds-services

%description
This package is a meta-package to ensure the installation of the
GDS LSCSOFT packages.

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Sat Jul 08 2017 Adam Mercer <adam.mercer@ligo.org> 1.4-1
- add dependencies on gds-frameio, gds-lowlatency, and gds-root

* Tue Apr 19 2016 Adam Mercer <adam.mercer@ligo.org> 1.3-1
- remove dependency on gds-web

* Tue Apr 19 2016 Adam Mercer <adam.mercer@ligo.org> 1.2-1
- remove dependencies on debuginfo packages

* Wed Aug 19 2015 Adam Mercer <adam.mercer@ligo.org> 1.1-1
- initial version
