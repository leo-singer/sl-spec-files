Name: lscsoft-pylal
Version: 1.1
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: PyLAL meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: python-pylal

%description
This package is a metapackage to ensure the installation of the PyLAL
LSCSOFT package.

%install

%files

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Fri Apr 15 2016 Adam Mercer <adam.mercer@ligo.org> 1.1-1
- don't depend on debuginfo packages

* Tue Aug 18 2015 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial version
