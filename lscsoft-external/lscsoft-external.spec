Name: lscsoft-external
Version: 2.37
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: LSCSoft External meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: ElectricFence
Requires: ImageMagick
Requires: R
Requires: R-devel
Requires: cgdb
Requires: clang
Requires: clang-analyzer
Requires: ctags
Requires: ddd
Requires: doxygen
Requires: duc
Requires: environment-modules
Requires: fftw
Requires: fftw-devel
Requires: fftw-static
Requires: fuse-sshfs
Requires: git-all >= 1.8.2
Requires: git-annex
Requires: git-lfs
Requires: glibc-static
Requires: graphviz
Requires: gsl
Requires: gsl-devel
Requires: gsl-static
Requires: highlight
Requires: htop
Requires: inotify-tools
Requires: java-1.8.0-openjdk
Requires: java-1.8.0-openjdk-devel
Requires: java-1.8.0-openjdk-javadoc
Requires: jsoncpp-devel
Requires: libxml2-static
Requires: lscsoft-external-cbc
Requires: lscsoft-external-octave
Requires: lscsoft-external-omega
Requires: lscsoft-external-periodic
Requires: lscsoft-external-python
Requires: msttcore-fonts-installer
Requires: openblas-devel
Requires: perf
Requires: qgit
Requires: root
#Requires: root-multiproc
Requires: root-physics
Requires: root-python
Requires: screen
Requires: swig
Requires: swig-doc
Requires: texlive-multirow
Requires: texlive-sectsty
Requires: texlive-tocloft
Requires: texlive-xtab
Requires: tmux
Requires: tree
Requires: valgrind
Requires: xorg-x11-server-Xvfb
Requires: xz
Requires: zlib-static
Requires: zsh

%description
This package is a meta-package to ensure the installation of the
all external packages required by LSCSoft.

%install

%files

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Mon Nov 20 2017 Adam Mercer <adam.mercer@ligo.org> 2.37-1
- actually include dependency on msttcore-fonts-installer

* Mon Nov 20 2017 Adam Mercer <adam.mercer@ligo.org> 2.36-1
- add msttcore-fonts-installer
- https://bugs.ligo.org/redmine/issues/5948

* Wed Mar 22 2017 Adam Mercer <adam.mercer@ligo.org> 2.35-1
- add htop
- https://bugs.ligo.org/redmine/issues/5373

* Sat Feb 04 2017 Adam Mercer <adam.mercer@ligo.org> 2.34-1
- switch openblas dependency to openblas-devel

* Sat Feb 04 2017 Adam Mercer <adam.mercer@ligo.org> 2.33-1
- add openblas, requested by Chris Biwer for pycbc

* Tue Nov 29 2016 Adam Mercer <adam.mercer@ligo.org> 2.32-1
- add gsl-static
- https://bugs.ligo.org/redmine/issues/4719

* Thu Sep 29 2016 Adam Mercer <adam.mercer@ligo.org> 2.31-1
- add clang
- add clang-analyzer
- add fuse-sshfs

* Tue Jun 07 2016 Adam Mercer <adam.mercer@ligo.org> 2.30-1
- move scipy to lscsoft-external-python
- add jsoncpp-devel - requested by John Zweizig for GDS
- add texlive-{multirow,sectsty,tocloft,xtab} - requested by Duncan
  Meacher for ldas-tools

* Fri May 27 2016 Adam Mercer <adam.mercer@ligo.org> 2.29-1
- remove root-multiproc as this is ROOT6 only

* Fri May 27 2016 Adam Mercer <adam.mercer@ligo.org> 2.28-1
- moved python packages from lscsoft-external into lscsoft-external-python

* Fri May 27 2016 Adam Mercer <adam.mercer@ligo.org> 2.27-1
- add dependency on lscsoft-external-python
- https://sympa.ligo.org/wws/arc/daswg/2016-05/msg00186.html

* Fri May 27 2016 Adam Mercer <adam.mercer@ligo.org> 2.26-1
- add dependencies on root-multiproc and root-physics
- requested by Stuart Anderson via email on 2016/05/25
- https://sympa.ligo.org/wws/arc/daswg/2016-05/msg00089.html

* Tue Apr 26 2016 Adam Mercer <adam.mercer@ligo.org> 2.25-1
- add dependencies on duc, environment-modules, git-annex, git-lfs, and xz

* Tue Apr 26 2016 Adam Mercer <adam.mercer@ligo.org> 2.24-1
- use versioned git dependency for git-lfs requirements

* Tue Apr 26 2016 Adam Mercer <adam.mercer@ligo.org> 2.23-1
- switch back to system git, don't force use of ius

* Mon Apr 18 2016 Adam Mercer <adam.mercer@ligo.org> 2.22-1
- remove python-sqlite2

* Mon Apr 18 2016 Adam Mercer <adam.mercer@ligo.org> 2.21-1
- remove funtools, sextractor, and xautomation

* Fri Apr 15 2016 Adam Mercer <adam.mercer@ligo.org> 2.20-1
- remove root-debuginfo

* Fri Apr 15 2016 Adam Mercer <adam.mercer@ligo.org> 2.19-1
- remove dependencies on debuginfo packages
- switch git dependency to ius version
- switch to java-1.8.0-openjdk packages
- add explict swig dependency

* Fri Oct 09 2015 Adam Mercer <adam.mercer@ligo.org> 2.18-2
- add dd
- add git-all
- add glibc-static
- add java-1.7.0-openjdk
- add java-1.7.0-openjdk-devel
- add java-1.7.0-openjdk-javadoc
- add libxml2-static
- add perf
- add python-basemap-data-hires
- add python-basemap-examples
- add qgit
- add root
- add root-debuginfo
- add root-python
- add sextractor
- add swig-debuginfo
- add swig-doc
- add xautomation
- add zlib-static

* Wed Aug 19 2015 Adam Mercer <adam.mercer@ligo.org> 2.18-1
- initial version
