Name: lscsoft-external-python
Version: 1.7
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: LSCSoft External Python meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: GitPython
Requires: astropy-tools
Requires: h5py
Requires: ipython
Requires: numpy
Requires: python-argparse
Requires: python-basemap
Requires: python-basemap-data-hires
Requires: python-basemap-examples
Requires: python-matplotlib
Requires: python-pip
Requires: python-sqlobject
Requires: python-virtualenv
Requires: python2-rpm-macros
Requires: python3-rpm-macros
Requires: python34
Requires: python34-crypto
Requires: python34-devel
Requires: python34-libs
Requires: python34-nose
Requires: python34-numpy
Requires: python34-numpy-f2py
Requires: python34-pip
Requires: python34-setuptools
Requires: python34-setuptools_scm
Requires: python34-virtualenv
Requires: scipy
Requires: sympy

%description
This package is a meta-package to ensure the installation of external
Python packages required by various analysis codes

%install

%files

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Fri Dec 15 2017 Adam Mercer <adam.mercer@ligo.org> 1.7-1
- add python34-pip and python34-virtualenv
- https://bugs.ligo.org/redmine/issues/6056

* Fri Jun 16 2017 Adam Mercer <adam.mercer@ligo.org> 1.6-1
- python3-pkgversion-macros has been retired and replaced with python3-rpm-macros

* Thu Jun 01 2017 Adam Mercer <adam.mercer@ligo.org> 1.5-1
- add sympy
- https://bugs.ligo.org/redmine/issues/5564

* Wed Apr 19 2017 Adam Mercer <adam.mercer@ligo.org> 1.4-1
- add python2-rpm-macros and python3-pkgversion-macros
- https://bugs.ligo.org/redmine/issues/5499

* Tue Mar 21 2017 Adam Mercer <adam.mercer@ligo.org> 1.3-1
- add astropy-tools
- https://bugs.ligo.org/redmine/issues/5370

* Tue Jun 07 2016 Adam Mercer <adam.mercer@ligo.org> 1.2-1
- moved scipy from lscsoft-external into lscsoft-external-python
- add h5py

* Fri May 27 2016 Adam Mercer <adam.mercer@ligo.org> 1.1-1
- moved python packages from lscsoft-external into lscsoft-external-python

* Fri May 27 2016 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial version
- https://sympa.ligo.org/wws/arc/daswg/2016-05/msg00186.html
