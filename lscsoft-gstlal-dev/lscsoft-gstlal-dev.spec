Name: lscsoft-gstlal-dev
Version: 1.0
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: GSTLAL development meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: cyrus-sasl-devel
Requires: doxygen
Requires: fftw-devel
Requires: gds-devel
Requires: gobject-introspection-devel
Requires: graphviz
Requires: gsl-devel
Requires: gstreamer1-devel
Requires: gstreamer1-plugins-base-devel
Requires: lal-devel
Requires: lal-python
Requires: lalburst-devel
Requires: lalinspiral-devel
Requires: lalinspiral-python
Requires: lalmetaio-devel
Requires: lalsimulation-devel
Requires: ldas-tools-framecpp-devel
Requires: nds2-client-devel
Requires: nds2-client-headers
Requires: numpy
Requires: orc
Requires: pygobject3-devel
Requires: python-devel

%description
This package is a meta-package to ensure the installation of the
dependencies required for developing GSTLAL.

%install

%files

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Wed Jul 06 2016 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial version
