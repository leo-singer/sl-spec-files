Name: lscsoft-glue
Version: 2.1
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: GLUE meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: glue
Requires: glue-common
Requires: glue-segments

%description
This package is a meta-package to ensure the installation of the
GLUE LSCSOFT packages.

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Thu Apr 14 2016 Adam Mercer <adam.mercer@ligo.org> 2.1-1
- don't depend on debuginfo packages

* Wed Aug 19 2015 Adam Mercer <adam.mercer@ligo.org> 2.0-1
- initial version
