# -*- mode: RPM-SPEC; indent-tabs-mode: nil -*-
%define _docdir %{_datadir}/doc/ldas-tools-%{version}

Summary: LDAS tools libframecpp toolkit runtime files
Name: ldas-tools-framecpp
Version: 2.5.8
Release: 2%{?dist}
License: ?
URL: http://www.ligo.caltech.edu
Group: Application/Scientific
BuildRoot: %{buildroot}
Source0: http://software.ligo.org/lscsoft/source/ldas-tools-framecpp-%{version}.tar.gz
Requires: ldas-tools-al
Buildrequires: autoconf
Buildrequires: automake
Buildrequires: doxygen
Buildrequires: libtool
Buildrequires: ldas-tools-al-devel
Buildrequires: openssl-devel
Buildrequires: pkgconfig
Buildrequires: python-devel
Buildrequires: numpy
Buildrequires: swig
Buildrequires: zlib-devel
Buildrequires: ldas-tools-al-devel >= 2.4.99

%description
This provides the runtime libraries for the framecpp library.

%package python
Requires: ldas-tools-framecpp
Requires: python
Requires: numpy
Group: Application/Scientific
Summary: Python extension for frameCPP
%description python
This provides the libraries needed to utilize the frameCPP library
from within Python

%package devel
Requires: ldas-tools-al-devel
Requires: ldas-tools-framecpp
Group: Development/Scientific
Summary: LDAS tools libframecpp toolkit development files
%description devel
This provides the develpement files the framecpp library.

%package doc
Group: Development/Scientific
Summary: LDAS tools libframecpp documentation
%description doc
This provides the documentation for the framecpp library.

%package c
Requires: ldas-tools-framecpp
Obsoletes: ldas-tools-framec
Obsoletes: ldas-tools-framecppc
Group: Application/Scientific
Summary: LDAS tools c wrapping of libframecpp
%description c
This provides the runtime libraries for the framecpp library.

%package c-devel
Requires: ldas-tools-framecpp-devel
Obsoletes: ldas-tools-framec-devel
Obsoletes: ldas-tools-framecppc-devel
Group: Development/Scientific
Summary: LDAS tools libframec toolkit development files
%description c-devel
This provides the develpement files the framecpp library.

%prep

%setup -q

%build

#------------------------------------------------------------------------
# This works around a bug in the current rpmbuild system whereby the
#   PKG_CONFIG_PATH is set by the system and does not allow for
#   user preference.
# This work around should be fixed in RH 7.1 or so
#------------------------------------------------------------------------
export PKG_CONFIG_PATH="${LDASTOOLSDEV_PKG_CONFIG_PATH:-}${LDASTOOLSDEV_PKG_CONFIG_PATH:+:}$PKG_CONFIG_PATH"

%configure --disable-warnings-as-errors --with-optimization=high --disable-tcl --enable-python --docdir=%{_docdir}
make V=1 %{?_smp_mflags}
make V=1 check

%install
rm -rf %{buildroot}
#--------------------------------------------------------------
# install lscsoft specific files
#--------------------------------------------------------------
make V=1 install DESTDIR=%{buildroot}
#--------------------------------------------------------------
# Removed unwanted libtool files
#--------------------------------------------------------------
find %{buildroot} -name \*.la -exec rm -f {} \;
rm -f %{buildroot}%{python_sitearch}/LDAStools/_*.a

%post
ldconfig
%postun
ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%exclude %{_libdir}/libframecppc.so*
%exclude %{_bindir}/framecpp_*.py
%{_bindir}/framecpp_*
%{_libdir}/libframecpp*.so.*

%files python
%defattr(-,root,root,-)
%{_bindir}/framecpp_*.py
%{python_sitearch}/LDAStools/frameCPP.py*
%{python_sitearch}/LDAStools/frgetvect_compat.py*
%{python_sitearch}/LDAStools/_frameCPP*.so*

%files devel
%defattr(-,root,root)
%exclude %{_libdir}/libframecppc.*a
%{_includedir}/framecpp
%{_libdir}/libframecpp*.*a
%{_libdir}/libframecpp*.so
%{_libdir}/pkgconfig/framecpp_common.pc
%{_libdir}/pkgconfig/framecpp3.pc
%{_libdir}/pkgconfig/framecpp4.pc
%{_libdir}/pkgconfig/framecpp6.pc
%{_libdir}/pkgconfig/framecpp7.pc
%{_libdir}/pkgconfig/framecpp8.pc
%{_libdir}/pkgconfig/framecpp.pc
%exclude %{_libdir}/libframecppc.*a
%exclude %{_libdir}/libframecppc.so

%files doc
%defattr(-,root,root)
%doc %{_docdir}/framecpp

%files c
%defattr(-,root,root,-)
%{_libdir}/libframecppc.so.*

%files c-devel
%defattr(-,root,root)
%{_includedir}/framecppc
%{_libdir}/libframecppc.*a
%{_libdir}/libframecppc.so
%{_libdir}/pkgconfig/framecppc.pc

%changelog
* Mon Feb 13 2017 Edward Maros <ed.maros@ligo.org>  - 2.5.6-1
- Built for new release

* Fri Jan 27 2017 Edward Maros <ed.maros@ligo.org> - 2.5.5-1
- Built for new release

* Sat Oct 22 2016 Edward Maros <ed.maros@ligo.org> - 2.5.4-1
- Built for new release

* Mon Oct 10 2016 Edward Maros <ed.maros@ligo.org> - 2.5.3-1
- Built for new release

* Wed Mar 23 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.4-1
- Made build be verbose

* Fri Mar 11 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.1-1
- Corrections for RPM build

* Thu Mar 03 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.0-1
- Breakout into separate source package

* Tue Oct 11 2011 Edward Maros <emaros@ligo.caltech.edu> - 1.19.13-1
- Initial build.
