%define gstreamername gstreamer_lscsoft

Name: gstlal-ugly
Version: 0.8.0
Release: 2.lscsoft
Summary: GSTLAL Experimental Supplements
License: GPL
Group: LSC Software/Data Analysis
Requires: gstlal >= 0.9.0 python >= 2.6 glue >= 1.48 glue-segments >= 1.48 python-pylal >= 0.7.0 fftw >= 3 %{gstreamername} >= 0.10.32 %{gstreamername}-plugins-base >= 0.10.32 %{gstreamername}-plugins-good >= 0.10.27 gstreamer_lscsoft-python >= 0.10.21 pygobject2 numpy scipy lal >= 6.14.0 lalmetaio >= 1.2.6 gsl ldas-tools-framecpp >= 2.3.3 gds-core >= 2.17.0 gds-crtools >= 2.17.0 gds-services >= 2.17.0 cyrus-sasl-lib nds2-client >= 0.11.2 orc >= 0.4.16
BuildRequires: gstlal-devel >= 0.9.0 python-devel >= 2.6 fftw-devel >= 3 %{gstreamername}-devel >= 0.10.32 %{gstreamername}-plugins-base-devel >= 0.10.32 pygobject2-devel lal-devel >= 6.14.0 lalmetaio-devel >= 1.2.6 gsl-devel ldas-tools-framecpp-devel >= 2.3.3 gds-devel >= 2.17.0 cyrus-sasl-devel nds2-client-devel >= 0.11.2 nds2-client-headers >= 0.11.2 orc >= 0.4.16 gds-services
Source: gstlal-ugly-%{version}.tar.gz
URL: https://www.lsc-group.phys.uwm.edu/daswg/projects/gstlal.html
Packager: Kipp Cannon <kipp.cannon@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package provides a variety of gstreamer elements for
gravitational-wave data analysis and some libraries to help write such
elements.  The code here sits on top of several other libraries, notably
the LIGO Algorithm Library (LAL), FFTW, the GNU Scientific Library (GSL),
and, of course, GStreamer.

This package contains the plugins and shared libraries required to run
gstlal-based applications.


%package devel
Summary: Files and documentation needed for compiling gstlal-based plugins and programs.
Group: LSC Software/Data Analysis
Requires: %{name} = %{version} gstlal-devel >= 0.9.0 python-devel >= 2.6 fftw-devel >= 3 %{gstreamername}-devel >= 0.10.32 %{gstreamername}-plugins-base-devel >= 0.10.32 pygobject2-devel lal-devel >= 6.14.0 lalmetaio-devel >= 1.2.6 gsl-devel nds2-client-headers >= 0.11.2
%description devel
This package contains the files needed for building gstlal-ugly based
plugins and programs.


%prep
%setup -q -n %{name}-%{version}


%build
. /opt/lscsoft/gst/gstenvironment.sh
%configure
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/gstlal/*
%{_libdir}/gstreamer-0.10/*.so
%{_libdir}/gstreamer-0.10/python/*
%{_prefix}/%{_lib}/python*/site-packages/gstlal

%files devel
%defattr(-,root,root)
#%{_libdir}/*.a
#%{_libdir}/*.so
%{_libdir}/gstreamer-0.10/*.a
