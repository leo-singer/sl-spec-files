%define gstreamername gstreamer1

Name: gstlal-ugly
Version: 0.99.2
Release: 2%{?dist}
Summary: GSTLAL Experimental Supplements
License: GPL
Group: LSC Software/Data Analysis
Requires: gstlal >= 0.99.4
Requires: python >= 2.7
Requires: glue >= 1.50
Requires: glue-segments >= 1.50
Requires: python-pylal >= 0.9.0
Requires: fftw >= 3
Requires: %{gstreamername} >= 1.2.4
Requires: %{gstreamername}-plugins-base >= 1.2.4
Requires: %{gstreamername}-plugins-good >= 1.2.4
Requires: %{gstreamername}-plugins-bad-free
Requires: numpy
Requires: scipy
Requires: lal >= 6.15.2
Requires: lalmetaio >= 1.2.6
Requires: gsl
Requires: ldas-tools-framecpp >= 2.5.0
Requires: gds-core >= 2.17.0
Requires: gds-crtools >= 2.17.0
Requires: gds-services >= 2.17.0
Requires: cyrus-sasl-lib
Requires: nds2-client >= 0.11.5
Requires: orc >= 0.4.16
Requires: python-%{gstreamername}
BuildRequires: doxygen  >= 1.8.3
BuildRequires: graphviz
BuildRequires: gstlal-devel >= 0.99.4
BuildRequires: python-devel >= 2.7
BuildRequires: fftw-devel >= 3
BuildRequires: %{gstreamername}-devel >= 1.2.4
BuildRequires: %{gstreamername}-plugins-base-devel >= 1.2.4
BuildRequires: lal-devel >= 6.15.2
BuildRequires: lalmetaio-devel >= 1.2.6
BuildRequires: gsl-devel
BuildRequires: ldas-tools-framecpp-devel >= 2.5.0
BuildRequires: gds-devel >= 2.17.0
BuildRequires: cyrus-sasl-devel
BuildRequires: nds2-client-devel >= 0.11.5
BuildRequires: nds2-client-headers >= 0.11.5
BuildRequires: orc >= 0.4.16
Source: gstlal-ugly-%{version}.tar.gz
URL: https://wiki.ligo.org/DASWG/GstLAL
Packager: Kipp Cannon <kipp.cannon@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package provides a variety of gstreamer elements for
gravitational-wave data analysis and some libraries to help write such
elements.  The code here sits on top of several other libraries, notably
the LIGO Algorithm Library (LAL), FFTW, the GNU Scientific Library (GSL),
and, of course, GStreamer.

This package contains the plugins and shared libraries required to run
gstlal-based applications.


%package devel
Summary: Files and documentation needed for compiling gstlal-based plugins and programs.
Group: LSC Software/Data Analysis
Requires: %{name} = %{version} gstlal-devel >= 0.99.4 python-devel >= 2.7 fftw-devel >= 3 %{gstreamername}-devel >= 1.2.4 %{gstreamername}-plugins-base-devel >= 1.2.4 lal-devel >= 6.15.2 lalmetaio-devel >= 1.2.6 gsl-devel nds2-client-headers >= 0.11.5
%description devel
This package contains the files needed for building gstlal-ugly based
plugins and programs.


%prep
%setup -q -n %{name}-%{version}


%build
%configure
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/gstlal/*
# there's no documentation
#%{_docdir}/gstlal-ugly-*
%{_libdir}/gstreamer-1.0/*.so
#%{_libdir}/gstreamer-1.0/python/*
%{_prefix}/%{_lib}/python*/site-packages/gstlal

%files devel
%defattr(-,root,root)
%{_libdir}/gstreamer-1.0/*.a
