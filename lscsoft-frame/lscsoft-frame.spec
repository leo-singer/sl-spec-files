Name: lscsoft-frame
Version: 2.3
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: Frame Library meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: libframe
Requires: libframe-devel
Requires: libframe-matlab
Requires: libframe-utils

%description
This package is a metapackage to ensure the installation of the Frame
library LSCSOFT packages.

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Thu Apr 14 2016 Adam Mercer <adam.mercer@ligo.org> 2.3-1
- don't depend on debuginfo packages

* Tue Aug 18 2015 Adam Mercer <adam.mercer@ligo.org> 2.2-1
- initial version
