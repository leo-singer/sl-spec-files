Name: lscsoft-ligotools
Version: 1.2
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: LIGOTools meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: ligotools-cache
Requires: ligotools-matlab
Requires: ligotools-scripts
Requires: ligotools-tconvert

%description
This package is a meta-package to ensure the installation of the
LIGOTools packages.

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Mon Jul 25 2016 Adam Mercer <adam.mercer@ligo.org> 1.2-1
- add ligotools-cache

* Sat Jul 02 2016 Adam Mercer <adam.mercer@ligo.org> 1.1-1
- add ligotools-matlab

* Tue Jun 14 2016 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial version
