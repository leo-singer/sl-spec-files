Summary: Utilities for obtaining short-lived proxy certificates for LIGO
Name: ligo-proxy-utils
Version: 1.2.1
Release: 2%{?dist}
Source0: ligo-proxy-utils-%{version}.tar.gz
License: Unknown
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Url: https://wiki.ligo.org/AuthProject
Requires: globus-proxy-utils >= 3.10, bash, curl >= 7.19.7-26, libxslt >= 1.1.26-2, openssl >= 1.0.0-20, osg-ca-certs >= 1.40

%description
%{summary}

%prep
%setup -q 

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/bin
install --mode=0755 ligo-proxy-init $RPM_BUILD_ROOT/usr/bin/ligo-proxy-init

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(0755,root,root,-)
/usr/bin/ligo-proxy-init

%changelog
* Mon Jul 20 2015 Paul Hopkins <paul.hopkins@ligo.org> - 1.2.1-1
- Added automatic failover for LIGO IdP servers
* Fri Jun 06 2014 Adam Mercer <adam.mercer@ligo.org) - 1.0.1-1
- Curl now asks for password directly
- Clears (DY)LD_LIBRARY_PATH environment variables
- Explicitly sets umask
- Checks user is not Virgo member
- Minor bugfixes
* Fri Feb 22 2013 Scott Koranda <scott.koranda@ligo.org> - 1.0.0-1
- Initial version.

