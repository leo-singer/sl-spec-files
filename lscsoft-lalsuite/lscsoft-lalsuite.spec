Name: lscsoft-lalsuite
Version: 2.9
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: LALSuite meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: lal
Requires: lal-debuginfo
Requires: lal-devel
Requires: lal-octave
Requires: lal-python
Requires: lalframe
Requires: lalframe-debuginfo
Requires: lalframe-devel
Requires: lalframe-octave
Requires: lalframe-python
Requires: lalmetaio
Requires: lalmetaio-debuginfo
Requires: lalmetaio-devel
Requires: lalmetaio-octave
Requires: lalmetaio-python
Requires: lalsimulation
Requires: lalsimulation-debuginfo
Requires: lalsimulation-devel
Requires: lalsimulation-octave
Requires: lalsimulation-python
Requires: lalxml
Requires: lalxml-debuginfo
Requires: lalxml-devel
Requires: lalxml-octave
Requires: lalxml-python
Requires: lalburst
Requires: lalburst-debuginfo
Requires: lalburst-devel
Requires: lalburst-octave
Requires: lalburst-python
Requires: laldetchar
Requires: laldetchar-debuginfo
Requires: laldetchar-devel
Requires: laldetchar-octave
Requires: laldetchar-python
Requires: lalinspiral
Requires: lalinspiral-debuginfo
Requires: lalinspiral-devel
Requires: lalinspiral-octave
Requires: lalinspiral-python
Requires: lalpulsar
Requires: lalpulsar-debuginfo
Requires: lalpulsar-devel
Requires: lalpulsar-octave
Requires: lalpulsar-python
Requires: lalinference
Requires: lalinference-debuginfo
Requires: lalinference-devel
Requires: lalinference-octave
Requires: lalinference-python
Requires: lalstochastic
Requires: lalstochastic-debuginfo
Requires: lalstochastic-devel
Requires: lalstochastic-octave
Requires: lalstochastic-python
Requires: lalapps
Requires: lalapps-debuginfo
Requires: lalsuite-extra

%description
This package is a meta-package to ensure the installation of the
LALSuite LSCSOFT packages.

%install

%files

%changelog
* Wed Aug 19 2015 Adam Mercer <adam.mercer@ligo.org> 2.9-1
- initial version
