Name: lscsoft-ldas-tools-dev
Version: 1.0
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: LDAS-Tools Development meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: autoconf
Requires: automake
Requires: bison
Requires: doxygen
Requires: flex
Requires: libtool
Requires: openssl-devel
Requires: pkgconfig
Requires: python-devel
Requires: swig
Requires: zlib-devel

%description
This package is a meta-package to ensure the installation of packages
required for LDAS-Tools development.

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Wed Jun 22 2016 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial version
