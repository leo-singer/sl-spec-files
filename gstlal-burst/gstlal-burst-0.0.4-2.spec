%define gstreamername gstreamer_lscsoft

Name: gstlal-burst
Version: 0.0.4
Release: 2.lscsoft
Summary: GSTLAL Burst
License: GPL
Group: LSC Software/Data Analysis
Requires: gstlal-ugly >= 0.7.0 gstlal >= 0.8.0 python >= 2.6 glue >= 1.46 glue-segments >= 1.46 python-pylal >= 0.5.0 %{gstreamername} >= 0.10.32 %{gstreamername}-plugins-base >= 0.10.32 %{gstreamername}-plugins-good >= 0.10.27 gstreamer_lscsoft-python >= 0.10.21 pygobject2 numpy scipy lal >= 6.13.0 lalmetaio >= 1.2.0 lalburst >= 1.2.2
BuildRequires: gstlal-devel >= 0.8.0 python-devel >= 2.6 fftw-devel >= 3 %{gstreamername}-devel >= 0.10.32 %{gstreamername}-plugins-base-devel >= 0.10.32 pygobject2-devel lal-devel >= 6.13.0 lalmetaio-devel >= 1.2.0
Conflicts: gstlal-ugly < 0.6.0
Source: gstlal-burst-%{version}.tar.gz
URL: https://www.lsc-group.phys.uwm.edu/daswg/projects/gstlal.html
Packager: Chris Pankow <chris.pankow@gravity.phys.uwm.edu>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package contains the plugins and shared libraries required to run the gstlal burst (generic transient) pipeline.

%package devel
Summary: Files and documentation needed for compiling gstlal-based plugins and programs.
Group: LSC Software/Data Analysis
Requires: %{name} = %{version} gstlal-devel >= 0.8.0 gstlal-ugly-devel >= 0.7.0 python-devel >= 2.6 fftw-devel >= 3 %{gstreamername}-devel >= 0.10.32 %{gstreamername}-plugins-base-devel >= 0.10.32 pygobject2-devel lal-devel >= 6.13.0 lalmetaio-devel >= 1.2.0 lalsimulation-devel >= 1.2.2 lalburst-devel >= 1.2.2 gsl-devel
%description devel
This package contains the files needed for building gstlal-burst based plugins
and programs.

%prep
%setup -q -n %{name}-%{version}


%build
. /opt/lscsoft/gst/gstenvironment.sh
%configure
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_prefix}/%{_lib}/python*/site-packages/gstlal
%{_libdir}/gstreamer-0.10/*.so
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_libdir}/gstreamer-0.10/*.a
%{_includedir}/*
