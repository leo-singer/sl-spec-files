Name: lscsoft-metaio
Version: 2.3
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: MetaIO meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: libmetaio
Requires: libmetaio-devel
Requires: libmetaio-utils

%description
This package is a metapackage to ensure the installation of the MetaIO
LSCSOFT packages.

%install

%files

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Thu Apr 14 2016 Adam Mercer <adam.mercer@ligo.org> 2.3-1
- don't depend on debuginfo packages

* Tue Aug 18 2015 Adam Mercer <adam.mercer@ligo.org> 2.2-1
- initial version
