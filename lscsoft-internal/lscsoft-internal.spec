Name: lscsoft-internal
Version: 2.28
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: Core LSCSoft meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: dqsegdb
Requires: ligo-common
Requires: ligo-gracedb
Requires: ligo-lars
Requires: ligo-lvalert
Requires: ligo-lvalert-heartbeat
Requires: lscsoft-ligotools
Requires: lscsoft-frame
Requires: lscsoft-gds
Requires: lscsoft-glue
Requires: lscsoft-gstlal
Requires: lscsoft-lalsuite
Requires: lscsoft-ldas
Requires: lscsoft-metaio
Requires: lscsoft-nds2-client
Requires: lscsoft-pylal
Requires: python-pyRXP
Requires: python-voeventlib

%description
This package is a meta-package to ensure the installation of the
Core LSCSoft packages.

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Thu Mar 09 2017 Adam Mercer <adam.mercer@ligo.org> 2.28-1
- add ligo-lvarlert-heartbeat

* Wed Jul 06 2016 Adam Mercer <adam.mercer@ligo.org> 2.27-1
- reinstate lscsoft-gstlal dependency

* Fri Jun 17 2016 Adam Mercer <adam.mercer@ligo.org> 2.26-1
- add dependency on lscsoft-ldas meta-package

* Fri Jun 17 2016 Adam Mercer <adam.mercer@ligo.org> 2.25-1
- remove lscsoft-lalsuite-dev as this is now in the new
  lscsoft-internal-dev meta-package

* Tue Jun 14 2016 Adam Mercer <adam.mercer@ligo.org> 2.24-1
- switch to new lscsoft-ligotools meta-package

* Tue Jun 14 2016 Adam Mercer <adam.mercer@ligo.org> 2.23-1
- add ligotools
- Requested by Stuart Anderson 14 June 2016

* Wed Apr 20 2016 Adam Mercer <adam.mercer@ligo.org> 2.22-1
- reinstate lscsoft-nds2-client

* Tue Apr 19 2016 Adam Mercer <adam.mercer@ligo.org> 2.21-1
- reinstate lscsoft-gds

* Mon Apr 18 2016 Adam Mercer <adam.mercer@ligo.org> 2.20-1
- comment out lscsoft-gstlal again, nds2-client dependency

* Mon Apr 18 2016 Adam Mercer <adam.mercer@ligo.org> 2.19-1
- reinstate lscsoft-gstlal

* Fri Apr 15 2016 Adam Mercer <adam.mercer@ligo.org> 2.18-1
- comment out gstlal deps temporarily

* Fri Apr 15 2016 Adam Mercer <adam.mercer@ligo.org> 2.17-1
- comment out gds and nds2-client deps temporarily

* Thu Apr 14 2016 Adam Mercer <adam.mercer@ligo.org> 2.16-1
- don't depend on debuginfo packages

* Fri Oct 09 2015 Adam Mercer <adam.mercer@ligo.org> 2.15-2
- add python-pyRXP-debuginfo
- add python-voeventlib-debuginfo

* Wed Aug 19 2015 Adam Mercer <adam.mercer@ligo.org> 2.15-1
- initial version
