Name: lscsoft-auth
Version: 1.2
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: Auth Project meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: cyrus-sasl-gssapi
Requires: krb5-workstation
Requires: python-kerberos

%description
This package is a meta-package to ensure the installation of packages
required for the Auth Project.

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Thu Apr 26 2016 Adam Mercer <adam.mercer@ligo.org> 1.2-1
- add krb5-workstation dependency

* Wed Aug 19 2015 Adam Mercer <adam.mercer@ligo.org> 1.1-1
- initial version
