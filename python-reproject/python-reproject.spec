%global srcname reproject
%global sum Reproject astronomical images

Name:           python-%{srcname}
Version:        0.3.2
Release:        2%{?dist}
Summary:        %{sum}

License:        BSD and ASL 2.0 and Python
# Licensing breakdown
# In general: BSD, see licenses/LICENSE.rst
#
# Exceptions:
# Apache (2.0):
#    astropy_helpers/astropy_helpers/sphinx/themes/bootstrap-astropy/static/bootstrap-astropy.css
#
# PSF:
#    astropy_helpers/licenses/LICENSE_COPYBUTTON.rst
#    astropy_helpers/astropy_helpers/sphinx/themes/bootstrap-astropy/static/copybutton.js
#
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/r/%{srcname}/%{srcname}-%{version}.tar.gz
Patch0:         python-reproject-fix-testoptions.patch

BuildRequires:  python2-astropy
BuildRequires:  python2-devel

%description
%{sum}.

%package -n python2-%{srcname}
Summary:        %{sum}
Requires:       python2-astropy
Requires:       healpy

%description -n python2-%{srcname}
%{sum}.

%prep
%setup -q -n %{srcname}-%{version}
# Fix option for running tests @Fedora
%patch0 -p1

%build
%py2_build

%install
%py2_install


# Disabled tests for now as they require network access, will query upstream
#%%check
#%%{__python2} setup.py test


%files -n python2-%{srcname}
%license LICENSE.md
%doc CHANGES.md README.md
%{python2_sitearch}/*

%changelog
* Tue Jan 23 2018 Leo Singer <leo.singer@ligo.org> - 0.3.2-2
- Rebuilt for SL7. Remove Python 3 package. Use bundled version of
  astropy-helpers. Remove Recommends: and $python_provide directives,
  which are not supported in SL7.

* Sun Oct 22 2017 Christian Dersch <lupinix@mailbox.org> - 0.3.2-1
- new version

* Thu Oct 12 2017 Christian Dersch <lupinix@mailbox.org> - 0.3.1-7
- Recommend python-healpy

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.3.1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.3.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.3.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 0.3.1-3
- Rebuild for Python 3.6

* Fri Oct 07 2016 Christian Dersch <lupinix@mailbox.org> - 0.3.1-2
- Added license breakdown

* Tue Oct 04 2016 Christian Dersch <lupinix@mailbox.org> - 0.3.1-1
- Initial packaging

