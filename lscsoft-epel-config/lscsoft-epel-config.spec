%define    __yumdir /etc/yum.repos.d

Name:      lscsoft-epel-config
Version:   1.6
Release:   1%{?dist}
Summary:   Yum configuration for LSCSoft EPEL Repository.
License:   GPL
Group:     LSCSoft
Source:    %{name}-%{version}.tar.xz
URL:       https://git.ligo.org/packaging/sl-spec-files/tree/master/lscsoft-epel-config
Packager:  Adam Mercer <adam.mercer@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Requires:  yum
BuildArch: noarch

%description
This RPM installs the required repository configuration files for
accessing the LSCSoft EPEL repository.

%prep
%setup -q

%build

%install
mkdir -p %{buildroot}/%{__yumdir}
%{__install} lscsoft-epel.repo %{buildroot}/%{__yumdir}

%post
echo
echo "Please run 'yum clean all' followed by 'yum repolist'"
echo "to ensure that the LSCSoft EPEL repository is accessible"
echo

%postun
echo
echo "Please run 'yum clean all' followed by 'yum repolist'"
echo "to ensure that the LSCSoft EPEL repository has been"
echo "successfully removed from the yum configuration"
echo

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -rf ${RPM_BUILD_ROOT}
rm -rf ${RPM_BUILD_DIR}/%{name}-%{version}

%files
%defattr(0644,root,root)
%{__yumdir}/lscsoft-epel.repo

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Tue Oct 24 2017 Adam Mercer <adam.mercer@ligo.org> 1.6-1
- remove exclusion of globus and gsi-openssh packages

* Mon Apr 24 2017 Adam Mercer <adam.mercer@ligo.org> 1.5-1
- exclude globus and gsi-openssh packages

* Fri Feb 24 2017 Adam Mercer <adam.mercer@ligo.org> 1.4-1
- don't exclude ROOT6 packages

* Mon Jan 23 2017 Adam Mercer <adam.mercer@ligo.org> 1.3-1
- exclude python2-root package

* Mon Aug 22 2016 Adam Mercer <adam.mercer@ligo.org> 1.2-1
- exclude root packages

* Fri Aug 19 2016 Adam Mercer <adam.mercer@ligo.org> 1.1-1
- use $releasever for version, allows easier support for both SL6 and SL7

* Wed Sep 02 2015 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial release
