Name: lscsoft-all
Version: 2.5
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: LSCSoft meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: lscsoft-auth
Requires: lscsoft-dev
Requires: lscsoft-external
Requires: lscsoft-internal
Requires: lscsoft-internal-dev

%description
This package is a meta-package to ensure the installation of the
all LSCSOFT packages.

%install

%files

%changelog
* Tue Nov 28 2017 Adam Mercer <adam.mercer@ligo.org> 2.5-1
- add dependency on lscsoft-dev

* Fri Jun 17 2016 Adam Mercer <adam.mercer@ligo.org> 2.4-1
- remove dependency on lscsoft-ldas which is now in lscsoft-internal

* Fri Jun 17 2016 Adam Mercer <adam.mercer@ligo.org> 2.3-1
- add dependency on lscsoft-internal-dev

* Wed Aug 19 2015 Adam Mercer <adam.mercer@ligo.org> 2.2-1
- initial version
