%define    __yumdir /etc/yum.repos.d

Name:      lscsoft-grid-config
Version:   1.1
Release:   1%{?dist}
Summary:   Yum configuration for Condor and Pegasus repositories
License:   GPL
Group:     LSCSoft
Source:    %{name}-%{version}.tar.xz
URL:       https://git.ligo.org/packaging/sl-spec-files/blob/master/lscsoft-grid-config
Packager:  Adam Mercer <adam.mercer@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Requires:  yum
BuildArch: noarch

%description
This RPM installes the required repository configuration files for
accessing the HTCondor and Pegasus repositories.

%prep
%setup -q

%build

%install
mkdir -p %{buildroot}/%{__yumdir}
%{__install} htcondor.repo %{buildroot}/%{__yumdir}
%{__install} pegasus.repo %{buildroot}/%{__yumdir}

%post
echo
echo "Please run 'yum clean all' followed by 'yum repolist'"
echo "to ensure that the HTCondor and Pegasus repositories are accessible"
echo

%postun
echo
echo "Please run 'yum clean all' followed by 'yum repolist'"
echo "to ensure that the HTCondor and Pegasus repositories have been"
echo "successfully removed from the yum configuration"
echo

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -rf ${RPM_BUILD_ROOT}
rm -rf ${RPM_BUILD_DIR}/%{name}-%{version}

%files
%defattr(0644,root,root)
%{__yumdir}/htcondor.repo
%{__yumdir}/pegasus.repo

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Tue Dec 06 2016 Adam Mercer <adam.mercer@ligo.org> 1.1-1
- use LSCSoft Pegasus mirror

* Sat Aug 20 2016 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial release
