Name: lscsoft-external-octave
Version: 2.3
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: LSCSoft External Octave meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: octave
Requires: octave-devel

%description
This package is a meta-package to ensure the installation of the
all external octave packages required by LSCSoft.

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Wed Aug 19 2015 Adam Mercer <adam.mercer@ligo.org> 2.3-1
- initial version
