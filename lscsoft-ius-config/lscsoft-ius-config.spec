%define    __yumdir /etc/yum.repos.d

Name:      lscsoft-ius-config
Version:   1.1
Release:   1%{?dist}
Summary:   Yum configuration for LSCSoft IUS Repository.
License:   GPL
Group:     LSCSoft
Source:    %{name}-%{version}.tar.xz
URL:       https://git.ligo.org/packaging/sl-spec-files/tree/master/lscsoft-ius-config
Packager:  Adam Mercer <adam.mercer@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Requires:  yum
BuildArch: noarch

%description
This RPM installs the required repository configuration files for
accessing the LSCSoft IUS repository.

%prep
%setup -q

%build

%install
mkdir -p %{buildroot}/%{__yumdir}
%{__install} lscsoft-ius.repo %{buildroot}/%{__yumdir}

%post
echo
echo "Please run 'yum clean all' followed by 'yum repolist'"
echo "to ensure that the LSCSoft IUS repository is accessible"
echo

%postun
echo
echo "Please run 'yum clean all' followed by 'yum repolist'"
echo "to ensure that the LSCSoft IUS repository has been"
echo "successfully removed from the yum configuration"
echo

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -rf ${RPM_BUILD_ROOT}
rm -rf ${RPM_BUILD_DIR}/%{name}-%{version}

%files
%defattr(0644,root,root)
%{__yumdir}/lscsoft-ius.repo

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Wed Apr 13 2016 Adam Mercer <adam.mercer@ligo.org> 1.1-1
- use $releasever for version, allows easy support for both SL6 and SL7

* Mon Dec 14 2015 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial release
