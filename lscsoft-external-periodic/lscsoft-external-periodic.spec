Name: lscsoft-external-periodic
Version: 2.7
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: LSCSoft External Periodic meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: R
Requires: R-devel
Requires: cfitsio
Requires: cfitsio-devel
Requires: cfitsio-static
Requires: fftw
Requires: fftw-devel
Requires: fftw-static
Requires: gsl
Requires: gsl-devel
Requires: lscsoft-internal
Requires: numpy
Requires: python-matplotlib
Requires: python-sqlobject
Requires: scipy

%description
This package is a meta-package to ensure the installation of the
all external packages required by the Periodic LSCSoft packages.

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Mon Apr 18 2016 Adam Mercer <adam.mercer@ligo.org> 2.7-1
- remove dependencies of debuginfo packages
- remove python-sqlite2

* Wed Aug 19 2015 Adam Mercer <adam.mercer@ligo.org> 2.6-1
- initial version
