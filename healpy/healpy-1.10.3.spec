Summary: Healpix tools package for Python
Name: healpy
Version: 1.10.3
Release: 1%{?dist}
Source: https://files.pythonhosted.org/packages/source/h/%{name}/%{name}-%{version}.tar.gz
License: GPLv2
Group: Development/Libraries
Vendor: C. Rosset, A. Zonca <cyrille.rosset@apc.univ-paris-diderot.fr>
Packager: Leo Singer <leo.singer@ligo.org>, Edward Maros <ed.maros@ligo.org>
Requires: healpix_cxx cfitsio python-astropy numpy python-matplotlib python-six
Url: http://github.com/healpy
BuildRequires: python-devel python-setuptools pkgconfig healpix_cxx-devel >= 3.30.0 cfitsio-devel python-astropy numpy python-matplotlib python-six

# Allow setup.py to run with ancient Scientific Linux version of setuptools.
# Remove when Scientific Linux gets setuptools >= 3.2.
Patch: healpy.ancient-setuptools.patch

%description
Healpy provides a python package to manipulate healpix maps. It is based
on the standard numeric and visualisation tools for Python, Numpy and
matplotlib.

%prep
%setup -q
%patch

%build
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
python setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES

%check
python setup.py test

%clean
rm -rf $RPM_BUILD_ROOT

%files -f INSTALLED_FILES
%defattr(-,root,root)

%changelog
* Thu Dec 29 2016 Leo Singer <leo.singer@ligo.org> 1.10.3-1

- New upstream release

* Wed Dec 21 2016 Leo Singer <leo.singer@ligo.org> 1.10.2-1

- New upstream release

* Tue Nov 08 2016 Leo Singer <leo.singer@ligo.org> 1.10.1-1

- New upstream release

* Thu May 19 2016 Leo Singer <leo.singer@ligo.org> 1.9.1-1

- New upstream release

- Depend on python-astropy instead of pyfits for FITS reading and writing

- Enable unit tests

- Fix rpmlint warnings

* Thu Mar 31 2016 Edward Maros <ed.maros@ligo.org> 1.6.1-5

- Added dist to release tag

- Changed packager to Edward Maros
