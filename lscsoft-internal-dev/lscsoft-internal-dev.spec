Name: lscsoft-internal-dev
Version: 1.2
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: Core LSCSoft development meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: lscsoft-gstlal-dev
Requires: lscsoft-lalsuite-dev
Requires: lscsoft-ldas-tools-dev
Requires: lscsoft-nds2-client-dev

%description
This package is a meta-package to ensure the installation of packages
required for the development of LSC Software

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Wed Jul 06 2016 Adam Mercer <adam.mercer@ligo.org> 1.2-1
- add lscsoft-gstlal-dev

* Sat Jun 18 2016 Adam Mercer <adam.mercer@ligo.org> 1.1-1
- add lscsoft-ldas-tools-dev

* Fri Jun 17 2016 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial version
