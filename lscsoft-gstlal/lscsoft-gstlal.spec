Name: lscsoft-gstlal
Version: 1.9
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: GSTLAL meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: gstlal
#Requires: gstlal-burst
#Requires: gstlal-burst-devel
Requires: gstlal-calibration
Requires: gstlal-devel
Requires: gstlal-inspiral
Requires: gstlal-inspiral-devel
Requires: gstlal-ugly
Requires: gstlal-ugly-devel

%description
This package is a meta-package to ensure the installation of the
GSTLAL LSCSOFT packages.

%install

%files

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Wed Jul 06 2016 Adam Mercer <adam.mercer@ligo.org> 1.9-1
- only depend on gstlal packages

* Mon Apr 18 2016 Adam Mercer <adam.mercer@ligo.org> 1.8-1
- more fixes for the use of OS provided gstreamer packages

* Mon Apr 18 2016 Adam Mercer <adam.mercer@ligo.org> 1.7-1
- remove gst-plugins-{cairovis,math}

* Fri Apr 15 2016 Adam Mercer <adam.mercer@ligo.org> 1.6-1
- remove dependencies of debuginfo packages
- use OS provided gstreamer packages

* Wed Aug 19 2015 Adam Mercer <adam.mercer@ligo.org> 1.5-1
- initial version
