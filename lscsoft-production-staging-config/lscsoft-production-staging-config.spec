%define    __yumdir /etc/yum.repos.d

Name:      lscsoft-production-staging-config
Version:   1.1
Release:   1%{?dist}
Summary:   Yum configuration for LSCSoft Production staging repository
License:   GPL
Group:     LSCSoft
Source:    %{name}-%{version}.tar.xz
URL:       http://ligo-vcs.phys.uwm.edu/cgit/sl-metapackages/tree/lscsoft-production-staging-config
Packager:  Adam Mercer <adam.mercer@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Requires:  yum
BuildArch: noarch

%description
This RPM installed the required repository configuration files for
accessing the LSCSoft Production staging repository.

%prep
%setup -q

%build

%install
mkdir -p %{buildroot}/%{__yumdir}
%{__install} lscsoft-production-staging.repo %{buildroot}/%{__yumdir}

%post
echo
echo "Please run 'yum clean all' followed by 'yum repolist'"
echo "to ensure that the LSCSoft Production staging repository is accessible"
echo

%postun
echo
echo "Please run 'yum clean all' followed by 'yum repolist'"
echo "to ensure that the LSCSoft Production staging repository has been"
echo "successfully removed from the yum configuration"
echo

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -rf ${RPM_BUILD_ROOT}
rm -rf ${RPM_BUILD_DIR}/%{name}-%{version}

%files
%defattr(0644,root,root)
%{__yumdir}/lscsoft-production-staging.repo

# dates should be formatred using: 'date +"%a %b %d %Y"'
%changelog
* Mon Mar 07 2016 Adam Mercer <adam.mercer@ligo.org> 1.1-1
- use $releasever for version, allows easy support for both SL6 and SL7

* Fri Mar 06 2015 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial release
