Name: lscsoft-lalsuite-dev
Version: 1.9
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: LALSuite Development meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: autoconf
Requires: automake
Requires: bc
Requires: cfitsio-devel
Requires: chealpix-devel
Requires: doxygen
Requires: fftw-devel
Requires: fftw-static
Requires: git-lfs
Requires: glib2-devel
Requires: gsl-devel
Requires: hdf5-devel
Requires: hdf5-static
Requires: healpy >= 1.8.5
Requires: ldas-tools-framecpp-c-devel
Requires: libframe-devel
Requires: libmetaio-devel
Requires: libtool
Requires: libxml2-devel
Requires: numpy
Requires: octave-devel
Requires: openmpi-devel
Requires: pkgconfig
Requires: python-devel
Requires: python-seaborn
Requires: python-shapely
Requires: python-six
Requires: scipy
Requires: swig
Requires: texlive-bibtex

%description
This package is a meta-package to ensure the installation of packages
rrequired for LALSuite development.

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Sat Dec 09 2017 Adam Mercer <adam.mercer@ligo.org> 1.9-1
- add git-lfs

* Sat Jun 03 2017 Adam Mercer <adam.mercer@ligo.org> 1.8-1
- add doxygen

* Wed Dec 21 2016 Adam Mercer <adam.mercer@ligo.org> 1.7-1
- add python-seaborn
- add python-six

* Fri Oct 28 2016 Adam Mercer <adam.mercer@ligo.org> 1.6-1
- https://bugs.ligo.org/redmine/issues/4747
- add python-shapely

* Thu Jun 23 2016 Adam Mercer <adam.mercer@ligo.org> 1.5-1
- add packages required for LALSuite development
- cfitsio-devel
- chealpix-devel
- fftw-devel
- glib2-devel
- gsl-devel
- hdf5-devel
- healpy >= 1.8.5
- ldas-tools-framecpp-c-devel
- libframe-devel
- libmetaio-devel
- libxml2-devel
- numpy
- openmpi-devel
- python-devel
- scipy

* Thu May 05 2016 Adam Mercer <adam.mercer@ligo.org> 1.4-1
- add fftw-static and hdf5-static

* Tue Apr 26 2016 Adam Mercer <adam.mercer@ligo.org> 1.3-1
- add bc and texlive-bibtex to dependencies

* Thu Apr 14 2016 Adam Mercer <adam.mercer@ligo.org> 1.2-1
- add pkgconfig dependency

* Wed Aug 19 2015 Adam Mercer <adam.mercer@ligo.org> 1.1-1
- initial version
