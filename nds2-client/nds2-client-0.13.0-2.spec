#========================================================================
# NOTES:
#    The octave default is to check for a system supplied package.
#
# OCTIVE override:
#    You can specify to build without octive support by specifying
#      --without octive
#    when invoking rpmbuild.
#========================================================================
%define name 	nds2-client
%define version 0.13.0
%define release 2
%define _prefix /usr
%define _sysconfdir %{_prefix}/etc
%define _includedir %{_prefix}/include/%{name}
%define _pkgdocdir  %{_defaultdocdir}/%{name}

%define _use_internal_dependency_generator 0
%define __find_requires %{_builddir}/%{name}-%{version}/config/nds-find-requires

#========================================================================
#
#  Set up octave symbols as appropriate
#
%if %{?_without_octave:1}%{!?_without_octave:0}
%define octave_installed %(echo 0)
%else
%define octave_installed %(test -e /usr/bin/octave && echo 1 || echo 0)
%endif

%define octave_build_opts %{nil}

%if %{octave_installed}
%define _octdatadir %(octave-config --m-site-dir)
%define _octexecdir %(octave-config --oct-site-dir)
%define octave_build_opts pkgoctdatadir=%_octdatadir pkgoctexecdir=%_octexecdir
%endif
#========================================================================

#========================================================================
#
#           Set up python symbols as appropriate
#
%define python_installed %(echo 1)
%define python_build_opts %{nil}
#========================================================================

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Sanity checks
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#========================================================================
# Main spec file
#========================================================================
Name: 		%{name}
Summary: 	NDS2 Client interface
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	GPL
Group: 		LSC Software/Data Analysis
Source: 	%{name}-%{version}.tar.gz
Packager: 	John Zweizig (john.zweizig@ligo.org)
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
URL: 		https://www.lsc-group.phys.uwm.edu/daswg/wiki/NetworkDataServer2
BuildRequires: 	gcc, gcc-c++, glibc, automake, autoconf, libtool, m4, make
BuildRequires:	gawk, pkgconfig
BuildRequires:	sqlite-devel, swig >= 2.0.11
BuildRequires:	python-devel, numpy
BuildRequires:	java-1.7.0-openjdk-devel, octave-devel
BuildRequires:	cyrus-sasl-devel, cyrus-sasl-gssapi
BuildRequires:	libcurl-devel
BuildRequires:	fontconfig-devel
BuildRequires:	mesa-libGLU-devel
BuildRequires:	pcre-devel
%if 0%{?rhel} >= 7
BuildRequires:	gl2ps-devel
BuildRequires:	qrupdate-devel
BuildRequires:	arpack-devel
BuildRequires:	suitesparse-devel
%endif
Requires:       cyrus-sasl, cyrus-sasl-gssapi
Summary: 	DMT offline
Provides: 	%name
Obsoletes:	%name < %version
Prefix:		%_prefix

%description
The NDS2 client interface allow the user to down-load LIGO data from the V2 LIGO Network Data Servers.

%package devel
Group: Development/Scientific
Summary: NDS2 development headers and dosumentation
Requires: nds2-client-headers nds2-client cyrus-sasl-devel
%description devel
This package supports development using the nds2 client protocol.

%package man
Group: Development/Scientific
Summary: NDS2 client man pages files
Requires: nds2-client
BuildArch: noarch
%description man
This package contains the man pages for the nds2-client package and its
high-level language extensions.

%package java
Group: Development/Scientific
Summary: Java extensions for NDS2
Requires: nds2-client
%description java
This provides java wrappers for the nds2 client

%package octave
Group: Development/Scientific
Summary: Octave extensions for NDS2
Requires: nds2-client
%description octave
This provides extensions to octave to access an NDS2 server

%package python
Group: Development/Scientific
Summary: Python extensions for NDS2
Requires: nds2-client
%description python
This provides python wrappers for the nds2 client

%package headers
Group: Development/Scientific
Summary: NDS2 headers
BuildArch: noarch
%description headers
This provides a separate package to install the headers shared by the 64 and 32 bit versions.

#----------------------------------------------
# Get onto the fun of building the NDS software
#----------------------------------------------
%prep
%setup

%build
PKG_CONFIG_PATH=/usr/%{_lib}/pkgconfig
export PKG_CONFIG_PATH
%configure \
        --program-prefix=%{?_program_prefix} \
	--prefix=%{prefix} \
        --exec-prefix=%{_exec_prefix} \
        --bindir=%{_bindir} \
        --sbindir=%{_sbindir} \
        --sysconfdir=%{_sysconfdir} \
        --datadir=%{_datadir} \
        --includedir=%{_includedir} \
        --libdir=%{_libdir} \
        --libexecdir=%{_libexecdir} \
        --localstatedir=%{_localstatedir} \
        --sharedstatedir=%{_sharedstatedir} \
        --mandir=%{_mandir} \
        --infodir=%{_infodir} \
        --disable-doc \
	%{?_with_octave} \
	%{?_without_octave}
make %{octave_build_opts}
make check

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} %{octave_build_opts}

#----------------------------------------------
# Remove files that will not be packaged based
# on the arch type
#----------------------------------------------

#----------------------------------------------
# Do the noarch files
#----------------------------------------------
%files headers
%{_includedir}/*.h
%{_includedir}/*.hh

%files man
%{_mandir}

#----------------------------------------------
# Handle binary packages
#----------------------------------------------
%define platform_64_bit %(echo %{_libdir} | grep 64 && echo 1 || echo 0)

%files
%defattr(-,root,root)
%_bindir/*
%_libdir/libndsclient*.so*
%_libdir/libndsxxwrap*.so*
%_sysconfdir/*

%files devel
%_libdir/libndsclient*.a
%_libdir/libndsclient*.la
%_libdir/libndsxxwrap*.a
%_libdir/libndsxxwrap*.la
%_libdir/pkgconfig/nds2-client.pc

#------------------------------------------------------------------------
# Java
#------------------------------------------------------------------------
%files java
%_libdir/libnds2JNI*
%_datadir/java/nds2

#------------------------------------------------------------------------
# Octave
#------------------------------------------------------------------------
%if %{octave_installed}
%files octave
%{_octexecdir}
%endif

#------------------------------------------------------------------------
# Python
#------------------------------------------------------------------------
%if %{python_installed}
%files python
%{python_sitearch}/
%{python_sitelib}/
%endif
