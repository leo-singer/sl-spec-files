Name: lscsoft-nds2-client-dev
Version: 1.4
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: nds2-client Development meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: arpack-devel
Requires: autoconf
Requires: automake
Requires: cmake
Requires: cmake3
Requires: cyrus-sasl-devel
Requires: cyrus-sasl-gssapi
Requires: docbook5-schemas
Requires: docbook5-style-xsl
Requires: fontconfig-devel
Requires: gl2ps-devel
Requires: libstdc++-static
Requires: java-1.8.0-openjdk-devel
Requires: libcurl-devel
Requires: libtool
Requires: make
Requires: mesa-libGLU-devel
Requires: numpy
Requires: octave-devel
Requires: pcre-devel
Requires: pkgconfig
Requires: python-devel
Requires: qrupdate-devel
Requires: sqlite-devel
Requires: suitesparse-devel
Requires: swig >= 2.0.11

%description
This package is a meta-package to ensure the installation of packages
required for nds2-client development.

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Fri Jan 12 2018 Adam Mercer <adam.mercer@ligo.org> 1.3-1
- add libstdc++-static

* Wed Feb 08 2017 Adam Mercer <adam.mercer@ligo.org> 1.2-1
* add cmake3

* Thu Jan 12 2017 Adam Mercer <adam.mercer@ligo.org> 1.1-1
- add cmake

* Fri Jun 17 2016 Adam Mercer <adam.mercer@ligo.org> 1.0-1
- initial version
