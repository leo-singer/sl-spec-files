Name: lscsoft-external-omega
Version: 2.3
Release: 1%{?dist}
Group: LSCSoft
License: GPL
Summary: LSCSoft External Omega meta-package
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildArch: noarch

Requires: ImageMagick
Requires: java-1.8.0-openjdk-devel
Requires: moreutils
Requires: xorg-x11-server-Xvfb

%description
This package is a meta-package to ensure the installation of the
all external packages required by the Omega LSCSoft packages.

%install

%files

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Mon Apr 18 2016 Adam Mercer <adam.mercer@ligo.org> 2.3-1
- switch java dependency to java-1.8.0-openjdk-devel

* Wed Aug 19 2015 Adam Mercer <adam.mercer@ligo.org> 2.2-1
- initial version
